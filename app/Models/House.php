<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;
    
    public function Company() {
        return $this->belongsTo('App\Models\Company');
    }
    
    public function Room(){
        return $this->hasMany('App\Models\Room');
    }
}
