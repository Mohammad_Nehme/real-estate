<?php

namespace App\Http\Controllers;
use App\Models\House;
use App\Models\Company;
use App\Models\Room;
use App\Mail\Send_matchups_to_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
session_start();

class ListingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses=House::all();
        return view('Lisitings.index')->with(['houses' => $houses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendEmail()
    {
        $data=$_SESSION['id'];
        Mail::to(auth()->user()->email)->send(new Send_matchups_to_user($data));
        return redirect('/')->with('success','email sent succesfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $house=House::find($id);
        $company=Company::find($house->Company_id);
        $rooms=Room::find($house->Rooms_id);
        return view('Lisitings.show')->with(['house'=>$house,'rooms'=>$rooms,'company'=>$company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search_view(){
        return view('Lisitings.search');
    }

    public function search_result(Request $request){
        $this->validate($request,
        [   'Living_Room'=> 'required|max:20',
             'Kitchen' =>'required|max:20',
             'Balcony' =>'required|max:20' ,
             'Bedroom' =>'required|max:20' ,
             'Bathroom' =>'required|max:20' ,
             'Swimming' =>'required|max:20',    
             'Gym' =>'required|max:20' ,
             'Lounge' =>'required|max:20' ,

        ]);
        
        
        $rooms=Room::where([
            ['Living_Room','>=',$request['Living_Room']],
            ['Kitchen','>=',$request['Kitchen']],
            ['Balcony','>=',$request['Balcony']],
            ['Bedroom','>=',$request['Bedroom']],
            ['Bathroom','>=',$request['Bathroom']],
            ['Swimming_Pool','>=',$request['Swimming']],
            ['Gym','>=',$request['Gym']],
            ['Lounges','>=',$request['Lounge']],


        ])->get();
            return view('Lisitings.found')->with('rooms',$rooms);

    }
}
