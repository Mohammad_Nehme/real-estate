<?php 
 use App\Models\User;
 use App\Models\House;
if(auth()->user()->cid !='Null'){
   
    $Employees=User::where([
        'type' =>'Employee',
        'cid' => auth()->user()->cid
    ])->get();
}
$houses=House::where([
    'Company_id'=> auth()->user()->cid
])->get();
?> 
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(auth()->user()->type=="Company")
                    <div class="float-right">
                    <a href="{{route('Employees_register')}}">Create a new employee</a>&nbsp;
                    <a href="{{route('Create_listing')}}"> Create a new listing</a>
                    </div>
                    
                    @endif

                    @if(auth()->user()->type=="Employee")
                    <div class="float-right">
                    <a href="{{route('Create_listing')}}"> Create a new listing</a>
                    </div>
                    @endif

                    @auth
                    {{ __('You are logged in!') }}
                    
                    @else {{'Not logged in'}}
                    
                    @endauth
                    
                </div>
            </div>
            <br>
            @if(auth()->user()->type=="Company")
            <hr>
            <h4 class="text-center"> Your Employees</h4>
            <table class="table table-bordered table-hover">
                <th scope="col" >Name</th>
                <th scope="col">Email</th>
                <th scope="col">Created on</th>
                <th scope="col" >Edit</th>
                <th scope="col">Delete</th>
                
                @foreach ($Employees as $Employee)
                <tr>
                <td >{{$Employee->name}}</td>
                <td>{{$Employee->email}}</td>
                <td>{{$Employee->created_at}}</td>
                <td><button class="btn btn-primary"><a href="/Edit/Employee/{{$Employee->id}}" style="text-decoration: none; color:white;">Edit</a></button></td>
                <td><form action="/Delete/Employee/{{$Employee->id}}" method="POST"><button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this Employee?');"> @csrf Delete</button></form></td>
            </tr>
            @endforeach
            </table>
            @endif
            <br>
            <hr>
        @if (auth()->user()->type =="Company" || auth()->user()->type=="Employee")
        <h4 class="text-center"> Your Listings</h4>
        <table class="table table-bordered table-hover">
            <th scope="col" >Location</th>
            <th scope="col">type</th>
            <th scope="col">Created on</th>
            <th scope="col" >Edit</th>
            <th scope="col">Delete</th>
            
            @foreach ($houses as $house)
            <tr>
            <td >{{$house->location}}</td>
            <td>{{$house->type}}</td>
            <td>{{$house->created_at}}</td>
            <td><button class="btn btn-primary"><a href="/Edit/house/{{$house->id}}" style="text-decoration: none; color:white;">Edit</a></button></td>
            <td><form action="/Delete/house/{{$house->id}}" method="POST"><button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this Employee?');"> @csrf Delete</button></form></td>
        </tr>
        @endforeach
        </table>
            
        @endif
        </div>
    </div>
</div>
@endsection
