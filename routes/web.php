<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');




Route::get('/Register_c','Companies_Controller@create')->name('Companies_register');
Route::post('/Register/success','Companies_Controller@store')->name('Success');
Route::get('/Edit/Employee/{id}','Companies_Controller@edit');
Route::post('/Edited','Companies_Controller@update')->name('Employee_edit');
Route::post('/Delete/Employee/{id}','Companies_Controller@Destroy');
Route::get('/Register_e','Companies_Controller@create_employee')->name('Employees_register');
Route::post('/Register/Done','Companies_Controller@store_employee')->name('Employee_done');


Route::get('/Listings','EmployeesController@create')->name('Create_listing');
Route::post('/Listings/create','EmployeesController@store')->name('Listing_created');
Route::get('/Edit/house/{id}','EmployeesController@edit');
Route::post('/Edited/house','EmployeesController@update')->name('Listing_updated');
Route::post('/Delete/house/{id}','EmployeesController@destroy'); 

Route::get('/Show','ListingsController@index')->name('Listings_show');
Route::get('/lisitings/{id}','ListingsController@show');
Route::get('/Search','ListingsController@search_view')->name('Listings_search');
Route::post('/Search/Found','ListingsController@search_result')->name('Listing_search');
Route::post('/Search/Found/mail','ListingsController@sendEmail')->name('send_email');
Auth::routes();
Auth::routes(['verify' => true]);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
