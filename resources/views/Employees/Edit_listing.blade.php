<?php 
   foreach($rooms as $room) {
        $living_Room=$room->Living_Room;
        $Kitchen=$room->Kitchen;
        $balcony=$room->Balcony;
        $Bedroom=$room->Bedroom;
        $Bathroom=$room->Bathroom;
        $Swimming_pool=$room->Swimming_Pool;
        $Gym=$room->Gym;
        $Lounges=$room->Lounges;
        $Total_Number_of_Rooms=$room->Total_Number_Of_Rooms;
    }

?>


@extends('layouts.app')


@section('content')
    <h2 class="text-center"> Please Update this Form with <u><strong> Accurate Information </strong> </u> about the house you would like to Update </h2><br>

    <div class="container">
        <form action="{{route('Listing_updated')}}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id" value="{{$house->id}}">
            @csrf
        <div class="row">
        <div class="col-sm">
            <label for="Living_Rooms">Number of Living Rooms</label>
            <input type="text" name="Living_Room" id="Living Rooms" class="form-control"  value="{{$living_Room}}" placeholder="Put the value of 0 if something is not found ">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of kitchens</label>
            <input type="text" name="Kitchen" id="Living Rooms" class="form-control" value="{{$Kitchen}}" placeholder="Put the value of 0 if something is not found">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of Balconies</label>
            <input type="text" name="Balcony" id="Living Rooms" class="form-control" value="{{$balcony}}" placeholder="Put the value of 0 if something is not found ">
        </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bedroom" class="mt-2">Number of Bedrooms</label>
                <input type="text" name="Bedroom" id="Bedroom" class="form-control"  value="{{$Bedroom}}"placeholder="Put the value of 0 if something is not found">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Bathrooms</label>
                <input type="text" name="Bathroom" id="Bathroom" class="form-control" value="{{$Bathroom}}" placeholder="Put the value of 0 if something is not found">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Swimming Pools </label>
                <input type="text" name="Swimming" id="Bathroom" class="form-control" value="{{$Swimming_pool}}" placeholder="Put the value of 0 if something is not found">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of gyms</label>
                <input type="text" name="Gym" id="Bathroom" class="form-control" value="{{$Gym}}" placeholder="Put the value of 0 if something is not found ">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Lounges</label>
                <input type="text" name="Lounge" id="Bathroom" class="form-control" value="{{$Lounges}}" placeholder="Put the value of 0 if something is not found ">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Total number of rooms</label>
                <input type="text" name="Total" id="Bathroom" class="form-control" value="{{$Total_Number_of_Rooms}}" placeholder="Put the value of 0 if something is not found ">
            </div>
        </div>
        <hr>
        <div class="row">
            
            <div class="col-sm mt-3">
        <textarea  name="location"  class="form-control" placeholder="Address" >{{$house->location}}</textarea>
            </div>
            <div class="col-sm mt-3">
        <textarea  name="Description"  class="form-control" placeholder="Brief Description" >{{$house->Brief_description}}</textarea>
            </div>
        </div>
        <hr>
        <div class="row">
            
            <div class="col-sm mt-3">
                <label for="type">Type:</label>
        <input name="type" value="{{$house->type}}" type="text" class="form-control" placeholder="Please Specifiy the type of the listed propery , e.g:Apartment/Mansion/house" value="">
            </div>
            <div class="col-sm mt-5">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="Image">
                    <label class="custom-file-label" for="customFile">Replace Your Photo (Optional)," The old one will be deleted"</label>
                  </div>
            </div>
        </div>
        <br>

            
                <button type="submit" class="btn btn-primary text-center" >Submit</button><br>

            </form>
            </div>
    
   



@endsection