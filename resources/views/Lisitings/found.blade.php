<?php 
use App\Models\Company;
use App\Models\House;
?>

@extends('layouts.app')

@section('content')
@if ($rooms->count()!=0)
<p class="text-muted text-center"> These are the best matches for your search </p>
<div class="container">
    <div class="row">
      <?php $counter=0;
      $_SESSION['id']=array(); ?>
    @foreach ($rooms as $room)
        <?php $house=House::find($room->House_id);
               $Company=Company::find($house->Company_id);
              $_SESSION['id'][$counter]=[
                'id' => $house->id,
                'Location' => $house->location,
              ];
              $counter++;?>
              
    <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
         <img src="/storage/images/{{$house->images}}" class="img-responsive card-img-top " data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" style="height: 225px; width: 100%; display: block;" data-holder-rendered="true"></a>
          <div class="card-body">
            <b><p class="card-text">{{$house->Brief_description}}</p></b>
            <br>
            <a href="/lisitings/{{$house->id}}" class="card-link btn btn-primary">More info</a>
            <p href="" class="card-link float-right mt-2">Listed by {{$Company->name}}</a> 
            <div class="d-flex justify-content-between align-items-center">
            </div>
          </div>
          <div class="card-footer text-muted">
           Type:  {{$house->type}}
          </div>
        </div>
    </div>

  @endforeach
<form action="{{route('send_email')}}" method="POST">@csrf @method('POST') <button class="btn btn-primary" type="submit">Press here to send a copy of these results to <b> <u> {{auth()->user()->email}}</u></b></button></form>
</div>
</div>
  @else
  No house was found with these descriptions
@endif


@endsection
