@if (count($errors)>0)
    <ol>
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            <small><li>{{$error}}</li></small>
        </div>
    @endforeach
    </ol>
@endif


@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif
