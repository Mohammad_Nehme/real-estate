@extends('layouts.app')


@section('content')
    <h2 class="text-center"> Please Fill this Form with <u><strong> Accurate Information </strong> </u> about the house you would like to list </h2><br>

    <div class="container">
        <form action="{{route('Listing_created')}}" method="POST" enctype="multipart/form-data">
            @csrf
        <div class="row">
        <div class="col-sm">
            <label for="Living_Rooms">Number of Living Rooms</label>
            <input type="text" name="Living_Room" id="Living Rooms" class="form-control"  value="{{old('Living_Room')}}" placeholder="Put the value of 0 if something is not found ">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of kitchens</label>
            <input type="text" name="Kitchen" id="Living Rooms" class="form-control" value="{{old('Kitchen')}}" placeholder="Put the value of 0 if something is not found">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of Balconies</label>
            <input type="text" name="Balcony" id="Living Rooms" class="form-control" value="{{old('Balcony')}}" placeholder="Put the value of 0 if something is not found ">
        </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bedroom" class="mt-2">Number of Bedrooms</label>
                <input type="text" name="Bedroom" id="Bedroom" class="form-control"  value="{{old('Bedroom')}}"placeholder="Put the value of 0 if something is not found">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Bathrooms</label>
                <input type="text" name="Bathroom" id="Bathroom" class="form-control" value="{{old('Bathroom')}}" placeholder="Put the value of 0 if something is not found">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Swimming Pools </label>
                <input type="text" name="Swimming" id="Bathroom" class="form-control" value="{{old('Swimming')}}" placeholder="Put the value of 0 if something is not found">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of gyms</label>
                <input type="text" name="Gym" id="Bathroom" class="form-control" value="{{old('Gym')}}" placeholder="Put the value of 0 if something is not found ">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Lounges</label>
                <input type="text" name="Lounge" id="Bathroom" class="form-control" value="{{old('Lounge')}}" placeholder="Put the value of 0 if something is not found ">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Total number of rooms</label>
                <input type="text" name="Total" id="Bathroom" class="form-control" value="{{old('Total')}}" placeholder="Put the value of 0 if something is not found ">
            </div>
        </div>
        <hr>
        <div class="row">
            
            <div class="col-sm mt-3">
        <textarea  name="location"  class="form-control" placeholder="Address" >{{old('location')}}</textarea>
            </div>
            <div class="col-sm mt-3">
        <textarea  name="Description"  class="form-control" placeholder="Brief Description" >{{old('Description')}}</textarea>
            </div>
        </div>
        <hr>
        <div class="row">
            
            <div class="col-sm mt-3">
                <label for="type">Type:</label>
        <input name="type" type="text" class="form-control" placeholder="Please Specifiy the type of the listed propery , e.g:Apartment/Mansion/house" value="{{old('type')}}">
            </div>
            <div class="col-sm mt-5">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="Image">
                    <label class="custom-file-label" for="customFile">Choose the Cover Photo</label>
                  </div>
            </div>
        </div>
        <br>

            
                <button type="submit" class="btn btn-primary text-center" align="center">Submit</button><br>

            </form>
            </div>
    
   



@endsection