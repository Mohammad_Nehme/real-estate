<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rooms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('House_id');
            $table->integer('Living_Room');
            $table->integer('Kitchen');
            $table->integer('Balcony');
            $table->integer('Bedroom');
            $table->integer('Bathroom');
            $table->integer('Swimming_Pool');
            $table->integer('Gym');
            $table->integer('Lounges');
            $table->integer('Total_Number_Of_Rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Rooms');
    }
}
