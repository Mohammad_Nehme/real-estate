<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable 
{
    use HasFactory;

    protected $guard ='Company';
    protected $fillable = [
        'email',
        'password',
    ];


    public function House(){
        return $this->hasMany('App\Models\House');
     }

    public function employee(){
       return $this->hasMany('App\Models\Employee');
    }
}
