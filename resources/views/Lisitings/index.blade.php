<?php 
use App\Models\Company;
?>

@extends('layouts.app')

@section('content')
@if ($houses->count()!=0)
<div class="container">
    <div class="row">

    @foreach ($houses as $house)
        <?php $Company=Company::find($house->Company_id); ?>
    <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
         <img src="storage/images/{{$house->images}}" class="img-responsive card-img-top " data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" style="height: 225px; width: 100%; display: block;" data-holder-rendered="true"></a>
          <div class="card-body">
            <b><p class="card-text">{{$house->Brief_description}}</p></b>
            <br>
            <a href="lisitings/{{$house->id}}" class="card-link btn btn-primary">More info</a>
            <p href="" class="card-link float-right mt-2">Listed by {{$Company->name}}</a> 
            <div class="d-flex justify-content-between align-items-center">
            </div>
          </div>
          <div class="card-footer text-muted">
           Type:  {{$house->type}}
          </div>
        </div>
    </div>
    
  @endforeach
</div>
</div>
  @else
  123
@endif
         

@endsection