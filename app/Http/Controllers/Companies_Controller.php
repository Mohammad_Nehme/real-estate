<?php

namespace App\Http\Controllers;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class Companies_Controller extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth',['except'=>'create'],'verified');
    }

    public function create(){
        return view('Companies.register');
    }

    public function store(Request $request) {
        
        $user=new User;
        $company=new Company;
        
        $this->validate($request,
    [   'name' => ['required','string','max:255'],
        'email' => ['required','email','string','unique:companies,email'],
        'owner' => ['required','string'],
        'Phone' => ['required'],
        'Address'=>['required'],
        'password'=>['required','confirmed','min:8'],
    ]
    );
        // creating the company instance in the company table
    $company->name=$request['name'];
    $company->Address=$request['Address'];
    $company->Phone_Number=$request['Phone'];
    $company->Company_Owner=$request['owner'];
    $company->email=$request['email'];
    $company->Number_of_employees=0;
    $company->save();
    $get_company_id=Company::where('email','=',$request['email'])->firstOrFail();
    $cid=$get_company_id->id;

    // creating the company instance in the users table

    $user->name=$request['name'];
    $user->email=$request['email'];
    $user->password=Hash::make($request['password']);
    $user->type='Company';
    $user->cid=$cid;
    $user->save();

    return redirect('/')->with('success','Account created !');

}

public function edit($id){
   

    if(auth()->user()->type !='Company'){
        return back()->with('error','Unauthorized access');}
    
    $user=User::find($id);
    return view('Companies.Employees_edit')->with('user',$user);
}


public function update(Request $request){

    if(auth()->user()->type !='Company'){
        return back()->with('error','Unauthorized access');}

    $this->validate($request,
    [
        'name'=>'required|string',
        'email'=> 'required|email',
    ]);

    $user=User::find($request['id']);
    $user->name=$request['name'];
    $user->email=$request['email'];
    $user->save();
    return redirect('/')->with('success','Employee Updated');


}

public function Destroy($id){

    if(auth()->user()->type !='Company'){
        return back()->with('error','Unauthorized access');}

    $user=User::find($id);
    $company=Company::find($user->cid);
    $company->Number_of_employees-=1;
    $company->save();
    $user->delete();

    return redirect('/')->with('success','Employee D eleted');

}

public function create_employee(){
    if(auth()->user()->type !='Company'){
        return back()->with('error','Unauthorized access');}
        return view('Companies.Employees_create');
    

}
public function store_employee(Request $request){
    if(auth()->user()->type !='Company'){
        return back()->with('error','Unauthorized access');}
    
    $user=new user;
        
    $this->validate($request,
    [   
        'name'=>'required|string',
        'Phone'=> 'required|numeric',
        'email'=> 'required|email|unique:users,email',
        'password'=>'required|confirmed|min:8',
    ]);
    
    // creating an employee instance in the user table 
    
    $user->name=$request['name'];
    $user->email=$request['email'];
    $user->password=Hash::make($request['password']);
    $user->type="Employee";
    $user->cid=auth()->user()->cid;
    $user->save();
    $increment_number_of_employyes=Company::find(auth()->user()->cid);
    $increment_number_of_employyes->Number_of_employees+=1;
    $increment_number_of_employyes->save();

    return redirect('/')->with('success','Employee Created');
    
}

}

