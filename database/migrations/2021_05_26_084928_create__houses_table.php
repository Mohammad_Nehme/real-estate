<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Houses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('Company_id')->constrained('Companies');
            $table->foreignId('Rooms_id');
            $table->string('location');
            $table->string('type');
            $table->string('Brief_description');
            $table->string('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Houses');
    }
}
