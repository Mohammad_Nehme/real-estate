@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container">

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-sm-9">
            {{-- <div class="callout" id="callout" style="display:none">
              <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
              <span class="message"></span>
            </div> --}}
              <div class="row">
                <div class="col-sm-6">
                  <img src="/storage/images/{{$house->images}}" width="100%" >
                  <br><br>
                    
                  <div class="form-group">
                    <div class="input-group col-sm-5">
                    </div>
                    <br>
                    </div>  
                    </div>
                
                <div class="col-sm-6">
                  <h1 class="page-header">type : {{$house->type}}</h1>
                  <h3><b> Address:{{$house->location}}</b></h3>
                 
                  <p><b>Description: {{$house->Brief_description}}</b></p>
                  Rooms Available :
                  <small>@if($rooms->Living_Room>0) Living Rooms: {{$rooms->Living_Room}},@endif
                  @if($rooms->Kitchen>0) Kitchens: {{$rooms->Kitchen}},@endif
                  @if($rooms->Balcony>0) Balcony {{$rooms->Balcony}},@endif
                  @if($rooms->Bedroom>0) Bedrooms {{$rooms->Bedroom}},@endif
                  @if($rooms->Bathroom>0) Bathroom {{$rooms->Bathroom}},@endif
                  @if($rooms->Swimming_Pool>0) Swimming_Pool {{$rooms->Swimming_Pool}},@endif
                  @if($rooms->Gym>0) Gym{{$rooms->Gym}},@endif
                  @if($rooms->Lounges>0)Lounges{{$rooms->Lounges}}@endif</small>
                  <br><br>
<small> <b>This house is listed by {{$company->name}} , which is located at {{$company->Address}},and can be contacted at {{$company->Phone_Number}} ,or via email at {{$company->email}}</small></b>
<div class="input-group col-sm-11">
                    </div>
                </div>  
              </div>
            </div>
        </div>
      </section>
    </div>
  </div>



@endsection