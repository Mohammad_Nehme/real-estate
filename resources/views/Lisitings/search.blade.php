
@extends('layouts.app')


@section('content')
    <h2 class="text-center"> Please Fill this Form about the house you would like to search for </h2><br>

    <div class="container">
        <form action="{{route('Listing_search')}}" method="POST">
            @csrf
        <div class="row">
        <div class="col-sm">
            <label for="Living_Rooms">Number of Living Rooms</label>
            <input type="text" name="Living_Room" id="Living Rooms" class="form-control" placeholder="Put the value of 0 if something is not required ">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of kitchens</label>
            <input type="text" name="Kitchen" id="Living Rooms" class="form-control" placeholder="Put the value of 0 if something is not required">
        </div>
        <div class="col-sm">
            <label for="Living_Rooms">Number of Balconies</label>
            <input type="text" name="Balcony" id="Living Rooms" class="form-control" placeholder="Put the value of 0 if something is not required ">
        </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bedroom" class="mt-2">Number of Bedrooms</label>
                <input type="text" name="Bedroom" id="Bedroom" class="form-control"  placeholder="Put the value of 0 if something is not required">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Bathrooms</label>
                <input type="text" name="Bathroom" id="Bathroom" class="form-control"  placeholder="Put the value of 0 if something is not required">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Swimming Pools </label>
                <input type="text" name="Swimming" id="Bathroom" class="form-control"  placeholder="Put the value of 0 if something is not required">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of gyms</label>
                <input type="text" name="Gym" id="Bathroom" class="form-control" placeholder="Put the value of 0 if something is not required ">
            </div>
            <div class="col-sm">
                <label for="Bathroom" class="mt-2">Number of Lounges</label>
                <input type="text" name="Lounge" id="Bathroom" class="form-control" placeholder="Put the value of 0 if something is not required ">
            </div>
        </div>
        <hr>
       

            
                <button type="submit" class="btn btn-primary text-center">Search</button><br>

            </form>
            </div>
    
   



@endsection