-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2021 at 01:57 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `real_estate`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone_Number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Company_Owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Number_of_employees` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `Address`, `Phone_Number`, `Company_Owner`, `Number_of_employees`, `email`, `created_at`, `updated_at`) VALUES
(7, 'Mohammad Nehme', 'Saint therese', '76072499', 'Mohammad Nehme', 1, 'mohammadnehme0908@gmail.com', '2021-05-28 05:48:16', '2021-06-02 08:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE `houses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Company_id` bigint(20) UNSIGNED NOT NULL,
  `Rooms_id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Brief_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id`, `Company_id`, `Rooms_id`, `location`, `type`, `Brief_description`, `images`, `created_at`, `updated_at`) VALUES
(12, 7, 4, 'Saint Therese , Main Road', 'Apartment', 'Good House', 'logan_apartments.6_1622630172.jpg', '2021-05-30 07:03:38', '2021-06-02 07:36:13'),
(13, 7, 5, 'Beirut', 'Apartment', 'Very Good house with a good view', 'photo-1522708323590-d24dbb6b0267_1622630284.jpg', '2021-05-31 06:34:08', '2021-06-02 07:44:07'),
(14, 7, 6, 'Badaro', 'Mansion', 'Big Mansion', 'cretan-mansion_1622630341.jpg', '2021-05-31 06:36:03', '2021-06-02 07:45:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2021_05_26_083757_create__companies_table', 1),
(3, '2021_05_26_084525_create_employees_table', 1),
(4, '2021_05_26_084928_create__houses_table', 1),
(5, '2021_05_26_090450_create__rooms_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 2),
(7, '2021_05_26_111505_add_email_to_companies_table', 2),
(8, '2021_05_27_083733_add_timestamps_to_companies', 3),
(9, '2021_05_27_085042_add_password_to_companies_table', 4),
(10, '2021_05_28_082307_add_type_and_cid_and_eid_to_users', 5),
(11, '2021_05_30_095414_add_timestamps_to_rooms', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `House_id` bigint(20) UNSIGNED NOT NULL,
  `Living_Room` int(11) NOT NULL,
  `Kitchen` int(11) NOT NULL,
  `Balcony` int(11) NOT NULL,
  `Bedroom` int(11) NOT NULL,
  `Bathroom` int(11) NOT NULL,
  `Swimming_Pool` int(11) NOT NULL,
  `Gym` int(11) NOT NULL,
  `Lounges` int(11) NOT NULL,
  `Total_Number_Of_Rooms` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `House_id`, `Living_Room`, `Kitchen`, `Balcony`, `Bedroom`, `Bathroom`, `Swimming_Pool`, `Gym`, `Lounges`, `Total_Number_Of_Rooms`, `created_at`, `updated_at`) VALUES
(4, 12, 3, 1, 2, 3, 2, 0, 0, 1, 12, '2021-05-30 07:03:38', '2021-05-30 07:03:38'),
(5, 13, 1, 1, 1, 1, 1, 1, 1, 1, 8, '2021-05-31 06:34:08', '2021-05-31 06:34:08'),
(6, 14, 1, 1, 1, 1, 1, 1, 1, 1, 9, '2021-05-31 06:36:03', '2021-06-02 07:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'User',
  `cid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Null'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `type`, `cid`) VALUES
(1, 'Mohammad Nehme ltd', 'mohammadnehme0908@gmail.com', '2021-06-02 08:47:00', '$2y$10$m6JmgBbFUZAAePqVUBxEyuueVbJsJ0yDkHMgfrZONbK5IGZJSKPv.', NULL, '2021-05-26 07:26:28', '2021-06-02 08:47:00', 'Company', '7'),
(19, 'Mohammad Nehme', 'mohammadbaker.nehme@gmail.com', '2021-06-02 08:28:20', '$2y$10$E18jjDeWenF2lP2eMKfGGOJyPBAjwhxI4h9vRppNGKQyupYa.HEXC', NULL, '2021-06-02 08:24:49', '2021-06-02 08:28:20', 'Employee', '7'),
(20, 'New User', 'mohammadnehme0908@hotmail.com', '2021-06-02 08:50:37', '$2y$10$z2WT8FLwgfyPHKjr1pSYYevOKsdbAtOCW27bNrxLSdGo1AsyhtKbO', NULL, '2021-06-02 08:48:00', '2021-06-02 08:50:37', 'User', 'Null');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `houses_company_id_foreign` (`Company_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `houses`
--
ALTER TABLE `houses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `houses`
--
ALTER TABLE `houses`
  ADD CONSTRAINT `houses_company_id_foreign` FOREIGN KEY (`Company_id`) REFERENCES `companies` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
