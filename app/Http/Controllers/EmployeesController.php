<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Employee;
use App\Models\Company;
use App\Models\Room;
use App\Models\House;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class EmployeesController extends Controller
{

    public function __construct()
    {
        
        $this->middleware(['auth','verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->type=='Null'){
            return back()->with('error','Unauthorized access');
        }
        return view('Employees.Create_listing');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()->type=='Null'){
            return back()->with('error','Unauthorized access');
        }

        $this->validate($request,
        [   'Living_Room'=> 'required|max:20',
             'Kitchen' =>'required|max:20',
             'Balcony' =>'required|max:20' ,
             'Bedroom' =>'required|max:20' ,
             'Bathroom' =>'required|max:20' ,
             'Swimming' =>'required|max:20'    ,    
             'Gym' =>'required|max:20' ,
             'Lounge' =>'required|max:20' ,
             'Total' =>'required|max:20' ,
             'location' =>'required|max:255' ,
             'Description' =>'required|max:255',
             'type' =>'required|max:20',
             'Image' => 'required|image|max:2000'

        ]);
        
        
        $house=New house;
        $room=new Room;

        //Get File name with the extension
         $file_name_with_ext = $request->file('Image')->getClientOriginalName();
         // Get filename without the extension 
         $filename=pathinfo($file_name_with_ext,PATHINFO_FILENAME);
         // Get just the extensions
        $extension=$request->file('Image')->getClientOriginalExtension();
        //Filename to store
         $file_name_to_Store=$filename.'_'.time().'.'.$extension;
         //Upload Image
         $path=$request->file('Image')->storeAs('public/images',$file_name_to_Store);


        $house->Company_id=auth()->user()->cid;
        $house->Rooms_id=0;
        $house->location=$request['location'];
        $house->type=$request['type'];
        $house->images=$file_name_to_Store;
        $house->Brief_description=$request['Description'];
        $house->save();

        $idHouse=House::where(['Company_id' => auth()->user()->cid])->orderBy('created_at','desc')->first()->get();
        
        foreach($idHouse as $id){
            $house_id=$id->id;
            }
        
        $room->Living_Room=$request['Living_Room'];
        $room->Kitchen=$request['Kitchen'];
        $room->Balcony=$request['Balcony'];
        $room->Bedroom=$request['Bedroom'];
        $room->Bathroom=$request['Bathroom'];
        $room->Swimming_Pool=$request['Swimming'];
        $room->Gym=$request['Gym'];
        $room->Lounges=$request['Lounge'];
        $room->Total_Number_Of_Rooms=$request['Total'];
        $room->House_id=$house_id;
        $room->save();
        
        $idroom=Room::where(['House_id' => $house_id])->orderBy('created_at','desc')->first()->get();
       
        foreach($idroom as $room){
            $room_id=$room->id;
        }

        $id->Rooms_id=$room_id;
        $id->save();

        return redirect('/')->with('success','listing created');

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if(auth()->user()->type=='Null'){
            return back()->with('error','Unauthorized access');
        }

        $house=House::find($id);
        $rooms=Room::where(['House_id' => $id])->get();
        
        return view('Employees.Edit_listing')->with(['house'=>$house,'rooms'=>$rooms]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(auth()->user()->type=='Null'){
            return back()->with('error','Unauthorized access');
        }
        
        $this->validate($request,
        [   'Living_Room'=> 'required|max:20',
             'Kitchen' =>'required|max:20',
             'Balcony' =>'required|max:20' ,
             'Bedroom' =>'required|max:20' ,
             'Bathroom' =>'required|max:20' ,
             'Swimming' =>'required|max:20'    ,    
             'Gym' =>'required|max:20' ,
             'Lounge' =>'required|max:20' ,
             'Total' =>'required|max:20' ,
             'location' =>'required|max:255' ,
             'Description' =>'required|max:255',
             'type' =>'required|max:20',
             'Image' => 'Nullable|image|max:2000'

        ]);
        
        $house=House::find($request['id']);
        $room=Room::find($house->Rooms_id);

        if($request->hasFile('Image')){
        
        //Get File name with the extension
         $file_name_with_ext = $request->file('Image')->getClientOriginalName();
         // Get filename without the extension 
         $filename=pathinfo($file_name_with_ext,PATHINFO_FILENAME);
         // Get just the extensions
        $extension=$request->file('Image')->getClientOriginalExtension();
        //Filename to store
         $file_name_to_Store=$filename.'_'.time().'.'.$extension;
         //Upload Image
         $path=$request->file('Image')->storeAs('public/images',$file_name_to_Store);
         

        }
      

        $house->location=$request['location'];
        $house->type=$request['type'];
        $house->Brief_description=$request['Description'];
        
        if($request->hasFile('Image')){
        Storage::delete('public/images/'.$house->images);
        $house->images=$file_name_to_Store;}
        
        $house->save();

        $room->Living_Room=$request['Living_Room'];
        $room->Kitchen=$request['Kitchen'];
        $room->Balcony=$request['Balcony'];
        $room->Bedroom=$request['Bedroom'];
        $room->Bathroom=$request['Bathroom'];
        $room->Swimming_Pool=$request['Swimming'];
        $room->Gym=$request['Gym'];
        $room->Lounges=$request['Lounge'];
        $room->Total_Number_Of_Rooms=$request['Total'];
        $room->save();
        return redirect('/')->with('success','Listing Updated Succesfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
